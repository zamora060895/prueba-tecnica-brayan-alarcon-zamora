﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;

namespace Datos
{
    public class DRuleta
    {

        public bool insertar_usuario(EUsuario eobj)
        {
           
           Accesos objAccesos = new Accesos();
           List<Accesos.Parametro> parametros = new List<Accesos.Parametro>();
           parametros.Add(new Accesos.Parametro("@nombre", SqlDbType.VarChar, ParameterDirection.Input, eobj.nombre));
           parametros.Add(new Accesos.Parametro("@monto", SqlDbType.Decimal, ParameterDirection.Input, eobj.saldo));

           var _resultado = objAccesos.EjecutarEscalar(Conexion.Obtener_Cadena_Conexion_Base_Produccion(), "sp_insertar_usuario", parametros);

           return true;
        }

        public decimal obtener_saldo_nombre_usuario(string nombre)
        {

            Accesos objAccesos = new Accesos();
            List<Accesos.Parametro> parametros = new List<Accesos.Parametro>();
            parametros.Add(new Accesos.Parametro("@nombre", SqlDbType.VarChar, ParameterDirection.Input, nombre));

            var _resultado = objAccesos.EjecutarQuery(Conexion.Obtener_Cadena_Conexion_Base_Produccion(), "sp_obtener_saldo_nombre_usuario", parametros);
 
             
             if (_resultado.Tables[0]!= null)
             {
                DataRow obj_Row = _resultado.Tables[0].Rows[0];
                return Convert.ToDecimal(obj_Row.ItemArray[2]);
             }
              
            return 0;
        }

    }
}
