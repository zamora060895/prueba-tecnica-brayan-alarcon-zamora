﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Conexion
    {
        public static string Obtener_Cadena_Conexion_Base_Produccion()
        { 
            string _cadena_conexion_base = ConfigurationManager.AppSettings["AppBD"]; 

            //string _usuario = Decoder_BL.BL.Crypto.Decrypt(ConfigurationManager.AppSettings["user"]);
            //string _password = Decoder_BL.BL.Crypto.Decrypt(ConfigurationManager.AppSettings["pwd"]);
            //string _database = Decoder_BL.BL.Crypto.Decrypt(ConfigurationManager.AppSettings["db"]);
            //string _server = Decoder_BL.BL.Crypto.Decrypt(ConfigurationManager.AppSettings["server"]);

            string _usuario  = ConfigurationManager.AppSettings["user"];
            string _password = ConfigurationManager.AppSettings["pwd"];
            string _database = ConfigurationManager.AppSettings["db"];
            string _server   = ConfigurationManager.AppSettings["server"];

            return string.Format(_cadena_conexion_base, _usuario, _password, _database, _server);
        }

    }
}
