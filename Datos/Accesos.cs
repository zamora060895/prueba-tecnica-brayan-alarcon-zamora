﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Accesos
    {
        private SqlConnection oSqlConexion = null/* TODO Change to default(_) if this is not a reference type */;
        private SqlTransaction oTransaction = null/* TODO Change to default(_) if this is not a reference type */;

        public SqlTransaction Transaction
        {
            get
            {
                return oTransaction;
            }
        }

        public SqlConnection ObtenerConexionActual()
        {
            return oSqlConexion;
        }

        public void IniciarTransaction()
        {
            if (oSqlConexion != null && oSqlConexion.State == ConnectionState.Open)
                oTransaction = oSqlConexion.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (oTransaction != null)
                oTransaction.Commit();
        }

        public void RollBackTransaction()
        {
            if (oTransaction != null)
                oTransaction.Rollback();
        }

        public void DisposeTransaction()
        {
            if (oTransaction != null)
            {
                oTransaction.Dispose();
                oTransaction = null;
            }
        }

        public SqlConnection ObtenerConexionSQL(string cadenaConexion)
        {
            if (oSqlConexion != null && (oSqlConexion.State == ConnectionState.Open || oSqlConexion.State == ConnectionState.Broken))
                CerrarConexion();
            oSqlConexion = new SqlConnection(cadenaConexion);
            return oSqlConexion;
        }

        public void AbrirConexion()
        {
            if (oSqlConexion.State != ConnectionState.Open)
                oSqlConexion.Open();
        }

        public void CerrarConexion()
        {
            if (oSqlConexion != null && oSqlConexion.State == ConnectionState.Open)
            {
                oSqlConexion.Close();
                oSqlConexion.Dispose();
                oSqlConexion = null;
            }
        }

        public ComandoSQL CrearComandoSQL(string procedimiento, List<Parametro> parametros, Int64 tiempoLimite = -1)
        {
            SqlCommand oCommand = new SqlCommand();
            string nombreParamSalida = string.Empty;
            ComandoSQL oComandoSQL = default(ComandoSQL);

            {
                var withBlock = oCommand;
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    try
                    {
                        foreach (Parametro oItem in parametros)
                        {
                            SqlParameter oSqlParameter = new SqlParameter();
                            {
                                var withBlock1 = oSqlParameter;
                                withBlock1.ParameterName = oItem.Nombre;
                                withBlock1.SqlDbType = oItem.Dbtype;
                                withBlock1.Direction = oItem.Direccion;
                                if (oItem.Direccion != ParameterDirection.Output)
                                    withBlock1.Value = oItem.Valor;
                                else
                                    nombreParamSalida = oItem.Nombre.Trim();
                                if (oItem.Tamanio > 0)
                                    withBlock1.Size = oItem.Tamanio;
                            }
                            withBlock.Parameters.Add(oSqlParameter);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }

            oComandoSQL = new ComandoSQL(oCommand, 0, nombreParamSalida);
            return oComandoSQL;
        }

        public List<ComandoSQL> EjecutarMultipleNonQuery(string cadenaConexion, List<ComandoSQL> ListaComandoSQL, Int64 tiempoLimite = -1)
        {
            int resultado;
            int indice = 0;
            ComandoSQL oCommand = default(ComandoSQL);
            List<ComandoSQL> oListaResultado = new List<ComandoSQL>();

            ObtenerConexionSQL(cadenaConexion);
            foreach (ComandoSQL oItemCommand in ListaComandoSQL)
                oItemCommand.SqlCommand.Connection = ObtenerConexionActual();

            try
            {
                AbrirConexion();
                IniciarTransaction();
                foreach (ComandoSQL oItemCommand in ListaComandoSQL)
                {
                    oItemCommand.SqlCommand.Transaction = Transaction;
                    resultado = Convert.ToInt32(oItemCommand.SqlCommand.ExecuteNonQuery());
                    if (!string.IsNullOrEmpty(oItemCommand.ParametroSalida))
                    {
                        oCommand = oItemCommand;
                        oCommand.Resultado = Convert.ToInt32(oItemCommand.SqlCommand.Parameters[oItemCommand.ParametroSalida].Value);
                        oListaResultado.Add(oCommand);
                    }
                    else
                    {
                        oCommand = oItemCommand;
                        oCommand.Resultado = resultado;
                        oListaResultado.Add(oCommand);
                    }
                    indice += 1;
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                throw ex;
            }
            finally
            {
                CerrarConexion();
                foreach (ComandoSQL oItemCommand in ListaComandoSQL)
                {
                    oItemCommand.SqlCommand.Dispose();
                    //oItemCommand.SqlCommand = null/* TODO Change to default(_) if this is not a reference type */;
                }
                DisposeTransaction();
            }

            return oListaResultado;
        }

        public string EjecutarNonQuery(string cadenaConexion, string procedimiento, List<Parametro> parametros, Int64 tiempoLimite = -1)
        {
            SqlCommand oCommand = new SqlCommand();
            string afectados = "0";
            string nombreParamSalida = string.Empty;

            {
                var withBlock = oCommand;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion != ParameterDirection.Output)
                                withBlock1.Value = oItem.Valor;
                            else
                                nombreParamSalida = oItem.Nombre.Trim();
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }

            try
            {
                AbrirConexion();
                IniciarTransaction();
                oCommand.Transaction = Transaction;
                afectados = Convert.ToString(oCommand.ExecuteNonQuery());
                if (!string.IsNullOrEmpty(nombreParamSalida))
                    afectados = (string)oCommand.Parameters[nombreParamSalida].Value;

                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollBackTransaction();
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
                DisposeTransaction();
            }

            return afectados;
        }

        public string EjecutarNonQuerySinTransaccion(string cadenaConexion, string procedimiento, List<Parametro> parametros, Int64 tiempoLimite = -1)
        {
            SqlCommand oCommand = new SqlCommand();
            string afectados = "0";
            string nombreParamSalida = string.Empty;

            {
                var withBlock = oCommand;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion != ParameterDirection.Output)
                                withBlock1.Value = oItem.Valor;
                            else
                                nombreParamSalida = oItem.Nombre.Trim();
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }
            try
            {
                AbrirConexion();
                afectados = oCommand.ExecuteNonQuery().ToString();
                if (!string.IsNullOrEmpty(nombreParamSalida))
                    afectados = (string)oCommand.Parameters[nombreParamSalida].Value;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
            }

            return afectados;
        }

        public string EjecutarNonQuerySinTransaccion_ConSqlConnection(SqlConnection pSqlConnection, SqlTransaction pSqlTransaction, string procedimiento, List<Parametro> parametros, Int64 tiempoLimite = -1)
        {
            SqlCommand oCommand = new SqlCommand();
            string afectados = "0";
            string nombreParamSalida = string.Empty;

            {
                var withBlock = oCommand;
                withBlock.Connection = pSqlConnection;
                withBlock.Transaction = pSqlTransaction;
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion != ParameterDirection.Output)
                                withBlock1.Value = oItem.Valor;
                            else
                                nombreParamSalida = oItem.Nombre.Trim();
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }
            try
            {
                afectados = oCommand.ExecuteNonQuery().ToString();
                if (!string.IsNullOrEmpty(nombreParamSalida))
                    afectados = (string)oCommand.Parameters[nombreParamSalida].Value;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
            }

            return afectados;
        }

        // ---------------------------------------------------------------------------------------------------------------------------------
        public DataTable EjecutarDatatable(string cadenaConexion, string procedimiento, List<Parametro> parametros = null, Int64 tiempoLimite = -1, object variableSalida = null)
        {
            SqlCommand oCommand = new SqlCommand();
            SqlDataAdapter oDataAdapter = new SqlDataAdapter();
            DataTable oDataTable = new DataTable();
            string nombreParamSalida = string.Empty;
            {
                var withBlock = oCommand;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);
                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion == ParameterDirection.Output)
                                nombreParamSalida = oItem.Nombre;
                            withBlock1.Value = oItem.Valor;
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }
            try
            {
                AbrirConexion();
                oDataAdapter.SelectCommand = oCommand;
                oDataAdapter.Fill(oDataTable);
                if (variableSalida != null)
                    variableSalida = (string)oCommand.Parameters[nombreParamSalida].Value;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
                oDataAdapter.Dispose();
                oDataAdapter = null/* TODO Change to default(_) if this is not a reference type */;
            }
            if (oDataTable != null)
                return oDataTable;
            else
                return null/* TODO Change to default(_) if this is not a reference type */;
        }
        // ---------------------------------------------------------------------------------------------------------------------------------



        public DataSet EjecutarQuery(string cadenaConexion, string procedimiento, List<Parametro> parametros = null, Int64 tiempoLimite = -1, object variableSalida = null)
        {
            SqlCommand oCommand = new SqlCommand();
            SqlDataAdapter oDataAdapter = new SqlDataAdapter();
            DataSet oDataSet = new DataSet();
            string nombreParamSalida = string.Empty;
            {
                var withBlock = oCommand;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);
                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion == ParameterDirection.Output)
                                nombreParamSalida = oItem.Nombre;
                            withBlock1.Value = oItem.Valor;
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }
            try
            {
                AbrirConexion();
                oDataAdapter.SelectCommand = oCommand;
                oDataAdapter.Fill(oDataSet);
                if (variableSalida != null)
                    variableSalida = oCommand.Parameters[nombreParamSalida].Value;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
                oDataAdapter.Dispose();
                oDataAdapter = null/* TODO Change to default(_) if this is not a reference type */;
            }
            if (oDataSet != null)
                return oDataSet;
            else
                return null/* TODO Change to default(_) if this is not a reference type */;
        }

        public DataSet EjecutarQuery2(string cadenaConexion, string procedimiento, List<Parametro> parametros = null, Int64 tiempoLimite = -1, object variableSalida = null)
        {
            SqlCommand oCommand = new SqlCommand();
            SqlDataAdapter oDataAdapter = new SqlDataAdapter();
            DataSet oDataSet = new DataSet();
            string nombreParamSalida = string.Empty;

            {
                var withBlock = oCommand;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion == ParameterDirection.Output)
                                nombreParamSalida = oItem.Nombre;
                            withBlock1.Value = oItem.Valor;
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }

            try
            {
                AbrirConexion();
                oDataAdapter.SelectCommand = oCommand;
                oDataAdapter.Fill(oDataSet);
                if (variableSalida != null)
                    variableSalida = oCommand.Parameters[nombreParamSalida].Value;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
                oDataAdapter.Dispose();
                oDataAdapter = null/* TODO Change to default(_) if this is not a reference type */;
            }

            if (oDataSet != null)
                return oDataSet;
            else
                return null/* TODO Change to default(_) if this is not a reference type */;
        }

        public object EjecutarEscalar(string cadenaConexion, string procedimiento, List<Parametro> parametros = null, Int64 tiempoLimite = -1, object variableSalida = null)
        {
            SqlCommand oCommand = new SqlCommand();
            string nombreParamSalida = string.Empty;
            object resultado;

            {
                var withBlock = oCommand;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion == ParameterDirection.Output)
                                nombreParamSalida = oItem.Nombre;
                            withBlock1.Value = oItem.Valor;
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }

            try
            {
                AbrirConexion();
                resultado = oCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
            }

            return resultado;
        }

        public object EjecutarEscalarSinTransaccion_ConSqlConnection(SqlConnection pSqlConnection, SqlTransaction pSqlTransaction, string procedimiento, List<Parametro> parametros = null, Int64 tiempoLimite = -1, object variableSalida = null)
        {
            SqlCommand oCommand = new SqlCommand();
            string nombreParamSalida = string.Empty;
            object resultado;

            {
                var withBlock = oCommand;
                withBlock.Connection = pSqlConnection;
                withBlock.Transaction = pSqlTransaction;
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion == ParameterDirection.Output)
                                nombreParamSalida = oItem.Nombre;
                            withBlock1.Value = oItem.Valor;
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }

            try
            {
                resultado = oCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
            }

            return resultado;
        }


        public DataSet EjecutarProcedimiento(string cadenaConexion, string procedimiento, int tiempoLimite, params object[] parametros)
        {
            SqlCommand oCommand = new SqlCommand();
            SqlDataAdapter oDataAdapter = new SqlDataAdapter();
            DataSet oDataSet = new DataSet();

            {
                var withBlock = oCommand;
                DataSet oParametrosDataSet = ObtenerListaParametros(cadenaConexion, procedimiento);
                if (oParametrosDataSet != null && oParametrosDataSet.Tables[1].Rows.Count > 0)
                {
                    int oTotalFilas = oParametrosDataSet.Tables[1].Rows.Count;
                    DataView oDataView = oParametrosDataSet.Tables[1].DefaultView;
                    oDataView.Sort = "Param_order ASC";

                    for (int oIndex = 0; oIndex <= oTotalFilas - 1; oIndex++)
                        withBlock.Parameters.Add(new SqlParameter(oDataView[oIndex][0].ToString(), parametros[oIndex]));
                }

                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = tiempoLimite;
            }

            try
            {
                AbrirConexion();
                oDataAdapter.SelectCommand = oCommand;
                oDataAdapter.Fill(oDataSet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
                oDataAdapter.Dispose();
                oDataAdapter = null/* TODO Change to default(_) if this is not a reference type */;
            }

            if (oDataSet != null)
                return oDataSet;
            else
                return null/* TODO Change to default(_) if this is not a reference type */;
        }



        public object EjecutarProcedimientoEscalar(string cadenaConexion, string procedimiento, int tiempoLimite, params object[] parametros)
        {
            SqlCommand oCommand = new SqlCommand();
            object resultado;

            {
                var withBlock = oCommand;
                DataSet oParametrosDataSet = ObtenerListaParametros(cadenaConexion, procedimiento);
                if (oParametrosDataSet != null && oParametrosDataSet.Tables[1].Rows.Count > 0)
                {
                    int oTotalFilas = oParametrosDataSet.Tables[1].Rows.Count;
                    DataView oDataView = oParametrosDataSet.Tables[1].DefaultView;
                    oDataView.Sort = "Param_order ASC";

                    for (int oIndex = 0; oIndex <= oTotalFilas - 1; oIndex++)
                        withBlock.Parameters.Add(new SqlParameter(oDataView[oIndex][0].ToString(), parametros[oIndex]));
                }

                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = tiempoLimite;
            }

            try
            {
                AbrirConexion();

                resultado = oCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
            }

            return resultado;
        }


        public string EjecutarOnlyNonQuery(string cadenaConexion, string procedimiento, List<Parametro> parametros, string ParametroSalida = "", Int64 tiempoLimite = -1)
        {
            SqlCommand oCommand = new SqlCommand();
            string afectados = "0";

            string nombreParamSalida = string.Empty;

            {
                var withBlock = oCommand;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion != ParameterDirection.Output)
                                withBlock1.Value = oItem.Valor;
                            else
                                nombreParamSalida = oItem.Nombre.Trim();
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }

            try
            {
                AbrirConexion();
                afectados = oCommand.ExecuteNonQuery().ToString();
                if (!string.IsNullOrEmpty(nombreParamSalida))
                    ParametroSalida = (string)oCommand.Parameters[nombreParamSalida].Value;

                //ParametroSalida = oCommand.Parameters.Item(nombreParamSalida.ToString()).Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
            }

            return afectados;
        }

        public DataSet EjecutarQuery3(string cadenaConexion, string procedimiento, List<Parametro> parametros = null, string variableSalida = "", Int64 tiempoLimite = -1)
        {
            SqlCommand oCommand = new SqlCommand();
            SqlDataAdapter oDataAdapter = new SqlDataAdapter();
            DataSet oDataSet = new DataSet();
            string nombreParamSalida = string.Empty;

            {
                var withBlock = oCommand;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = procedimiento;
                withBlock.CommandType = CommandType.StoredProcedure;
                if (tiempoLimite > -1)
                    withBlock.CommandTimeout = Convert.ToInt32(tiempoLimite);

                if (parametros != null)
                {
                    foreach (Parametro oItem in parametros)
                    {
                        SqlParameter oSqlParameter = new SqlParameter();
                        {
                            var withBlock1 = oSqlParameter;
                            withBlock1.ParameterName = oItem.Nombre;
                            withBlock1.SqlDbType = oItem.Dbtype;
                            withBlock1.Direction = oItem.Direccion;
                            if (oItem.Direccion == ParameterDirection.Output)
                                nombreParamSalida = oItem.Nombre;
                            withBlock1.Value = oItem.Valor;
                            if (oItem.Tamanio > 0)
                                withBlock1.Size = oItem.Tamanio;
                        }
                        withBlock.Parameters.Add(oSqlParameter);
                    }
                }
            }

            try
            {
                AbrirConexion();
                oDataAdapter.SelectCommand = oCommand;
                oDataAdapter.Fill(oDataSet);
                if (variableSalida != null)
                    variableSalida = (string)(oCommand.Parameters[nombreParamSalida].Value);

                // variableSalida = oCommand.Parameters.Item(nombreParamSalida).Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand.Dispose();
                oCommand = null/* TODO Change to default(_) if this is not a reference type */;
                oDataAdapter.Dispose();
                oDataAdapter = null/* TODO Change to default(_) if this is not a reference type */;
            }

            if (oDataSet != null)
                return oDataSet;
            else
                return null/* TODO Change to default(_) if this is not a reference type */;
        }



        private DataSet ObtenerListaParametros(string cadenaConexion, string procedimiento)
        {
            SqlCommand oCommand2 = new SqlCommand();
            SqlDataAdapter oDataAdapter2 = new SqlDataAdapter();
            DataSet oDataSet2 = new DataSet();

            {
                var withBlock = oCommand2;
                withBlock.Connection = ObtenerConexionSQL(cadenaConexion);
                withBlock.CommandText = string.Format("sp_help {0}", procedimiento);
                withBlock.CommandType = CommandType.Text;
            }

            try
            {
                AbrirConexion();
                oDataAdapter2.SelectCommand = oCommand2;
                oDataAdapter2.Fill(oDataSet2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CerrarConexion();
                oCommand2.Dispose();
                oCommand2 = null/* TODO Change to default(_) if this is not a reference type */;
                oDataAdapter2.Dispose();
                oDataAdapter2 = null/* TODO Change to default(_) if this is not a reference type */;
            }

            if (oDataSet2 != null)
                return oDataSet2;
            else
                return null/* TODO Change to default(_) if this is not a reference type */;
        }


        public struct ComandoSQL
        {
            private SqlCommand _sqlcommand;
            private int _resultado;
            private string _parametroSalida;

            public ComandoSQL(SqlCommand oSqlCommand, int oResultado, string oParametroSalida)
            {
                _sqlcommand = oSqlCommand;
                _resultado = oResultado;
                _parametroSalida = oParametroSalida;
            }

            public SqlCommand SqlCommand
            {
                get
                {
                    return _sqlcommand;
                }
                set
                {
                    _sqlcommand = value;
                }
            }

            public int Resultado
            {
                get
                {
                    return _resultado;
                }
                set
                {
                    _resultado = value;
                }
            }

            public string ParametroSalida
            {
                get
                {
                    return _parametroSalida;
                }
                set
                {
                    _parametroSalida = value;
                }
            }
        }


        public struct Parametro
        {
            private string _nombre;
            private SqlDbType _dbtype;
            private ParameterDirection _direccion;
            private object _valor;
            private int _tamanio;

            public Parametro(string _nombre, SqlDbType _dbtype, ParameterDirection _direccion, object _valor, int _tamanio = 0)
            {
                this._nombre = _nombre;
                this._dbtype = _dbtype;
                this._direccion = _direccion;
                this._valor = _valor;
                this._tamanio = _tamanio;
            }

            public string Nombre
            {
                get
                {
                    return _nombre;
                }
                set
                {
                    _nombre = value;
                }
            }

            public SqlDbType Dbtype
            {
                get
                {
                    return _dbtype;
                }
                set
                {
                    _dbtype = value;
                }
            }

            public ParameterDirection Direccion
            {
                get
                {
                    return _direccion;
                }
                set
                {
                    _direccion = value;
                }
            }

            public object Valor
            {
                get
                {
                    return _valor;
                }
                set
                {
                    _valor = value;
                }
            }

            public int Tamanio
            {
                get
                {
                    return _tamanio;
                }
                set
                {
                    _tamanio = value;
                }
            }
        }
    }
}
