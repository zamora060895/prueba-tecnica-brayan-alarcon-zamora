﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Negocio;
using ExcelDataReader;
using System.Data;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
 
namespace WebReclamos.Controllers
{
    public class seguridadController : Controller
    {
        // GET: login
        public ActionResult login()
        { 
          return View();
        }
         
        [HttpGet]
        public ActionResult obtener_saldo_nombre_usuario(string nombre)
        {
            
            NRuleta nRuleta = new NRuleta();
            var saldo = nRuleta.obtener_saldo_nombre_usuario(nombre.ToUpper());

            return new JsonResult { Data = saldo, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult insertar_usuario(string model)
        {
            EUsuario obj = Newtonsoft.Json.JsonConvert.DeserializeObject<EUsuario>(model);

            NRuleta nRuleta = new NRuleta();

            obj.nombre = obj.nombre.ToUpper();
            var usuario = nRuleta.insertar_usuario(obj);

            return new JsonResult { Data = usuario, JsonRequestBehavior = JsonRequestBehavior.AllowGet };             
        }    

        [HttpGet]
        public ActionResult numero_color_aleatorio()
        {
            ENumeroColor objRandom = new ENumeroColor();

            Random r = new Random();
            //37 no se incluye
            var numero = r.Next(0, 37);

            var color = r.Next(0, 100) % 2 == 0 ? "black" : "red";

            objRandom.numero = numero;
            objRandom.color = color;
            objRandom.par = numero % 2 == 0 ? true : false;
             
            return new JsonResult { Data = objRandom, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}
