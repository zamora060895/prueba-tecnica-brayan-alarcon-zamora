(function () {
    'use strict';

    Vue.use(VeeValidate, {
        locale: 'es',
        dictionary: {
            es: {
                custom: {
                    usuario: {
                        required: 'El usuario no puede estar vac�o.'
                    },
                    contrasenia: {
                        required: 'La contrase�a no puede estar vac�o.'
                    }
                }
            }
        }
    });

    var app = new Vue({
        el: '#appLogin',
        data: {
            juegoIniciado: false,
            nombre: '',
            saldo: 0,
            apuesta: 0,

            color:'',
            numero: '',

            juegoGanado: false,
            colorResultado: '',
            numeroResultado: '',
            apuestaGanada: 0,

            historial: [],        
        },
        created: function () {
            if (!JSON.parse(localStorage.getItem('historialApuestas'))) {
                localStorage.setItem('historialApuestas', JSON.stringify([]));
            }            
        },
        methods: {
            empezarJuego(evt) {
                evt.preventDefault()
                this.juegoIniciado = true;

                console.log('nombre', this.nombre);
                console.log('saldo', this.saldo);
                console.log('apuesta', this.apuesta);
            },

            play(evt) {
                evt.preventDefault()
                var vm = this;
                 
                vm.juegoGanado = false;
                vm.colorResultado = "";
                vm.numeroResultado = "";
                vm.apuestaGanada = 0;

                if (!vm.color) {
                    console.log('Seleccionar color')
                    return;
                }

                vm.run_waitMe();
                 
                return axios.get('/seguridad/numero_color_aleatorio', {
                    params: {}
                }).then(function (response) {

                    //mostrar el reulstado de la busqueda
                    vm.colorResultado = response.data.color == 'red' ? 'Rojo' : 'Negro';
                    vm.numeroResultado = response.data.numero;

                   
                    if (vm.color && !vm.numero) {
                        if (vm.color == response.data.color) {
                            console.info('Apuesta por color');
                            console.info('ganaste la mitad de tu apuesta');
                            vm.apuestaGanada = parseFloat(vm.apuesta / 2);
                            vm.saldo = parseFloat(vm.saldo) + vm.apuestaGanada;
                            vm.juegoGanado = true;
                             
                        }
                    } else {

                        if (vm.numero == -1 || vm.numero == -2) {

                            //par
                            if (vm.numero == -2 && response.data.par && vm.color == response.data.color) {
                                console.info('Apuesta par');
                                console.info('ganaste el doble de tu apuesta');
                                vm.apuestaGanada = parseFloat(vm.apuesta * 2);
                                vm.saldo = parseFloat(vm.saldo) + vm.apuestaGanada;
                                vm.juegoGanado = true;
                            }

                            //impar
                            if (vm.numero == -1 && !response.data.par && vm.color == response.data.color) {
                                console.info('Apuesta impar');
                                console.info('ganaste el doble de tu apuesta');
                                vm.apuestaGanada = parseFloat(vm.apuesta * 2);
                                vm.saldo = parseFloat(vm.saldo) + vm.apuestaGanada;
                                vm.juegoGanado = true;
                            }
 
                        } else {

                            if (vm.color == response.data.color && vm.numero == response.data.numero) {
                                console.info('Apuesta por color y numero especifico');
                                console.info('ganaste el triple de tu apuesta');
                                vm.apuestaGanada = parseFloat(vm.apuesta * 3);
                                vm.saldo = parseFloat(vm.saldo) + vm.apuestaGanada;
                                vm.juegoGanado = true;
                            }
                        }                       
                    }

                    vm.historial = JSON.parse(localStorage.getItem('historialApuestas'));

                    let objHistorial = {
                        nombre: vm.nombre,
                        apuesta: vm.apuesta,
                        apuestaGanada: vm.apuestaGanada,
                    };
                     
                    vm.historial.push(objHistorial)

                    localStorage.setItem('historialApuestas', JSON.stringify(vm.historial));

                    $("#exampleModalTopCover").modal('show');

                }).catch(function (error) {
                        console.error("Ocurrio un error al intentar obtener datos aleatorios.")
                }).finally(function () {
                    EasyLoading.hide();
                });
                     
            },

            seleccionarColor(color) {
                this.color = color;               
            },
            seleccionarNumero(numero) {
                this.numero = numero;
            },

            empezarNuevoJuego() {
               this.juegoIniciado = false;
               this.nombre = '';
               this.saldo = 0;
               this.apuesta = 0;
               this.color = '';
               this.numero = '';
               this.juegoGanado = false;
               this.colorResultado = '';
               this.numeroResultado = '';
               this.apuestaGanada = 0;
               $('#exampleModalTopCover').modal('hide');
            },

            guardarSaldo() {

                let vm = this;
                 
                //iziToast.warning({
                //    title: 'Alerta',
                //    message: 'El porcentaje de comisi�n no puede ser vac�o',
                //    position: 'topRight',
                //    timeout: 3000
                //});
                 
                vm.run_waitMe();
                let formData = new FormData();

                let obj = {
                    "nombre": vm.nombre,
                    "saldo": parseFloat(vm.saldo),
                };

                formData.append("model", JSON.stringify(obj));

                return axios({
                    method: 'post',
                    url: '/seguridad/insertar_usuario',
                    data: formData,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(function (response) {

                    console.error(response.data)

                    return response.data;
                }).catch(function (error) {
                    console.error(error)
                }).finally(function () {
                    EasyLoading.hide();
                });
            },

            verHistorial(){
                $("#historialModalTopCover").modal('show');

                this.historial = JSON.parse(localStorage.getItem('historialApuestas'));
            },

            obtenerSaldo() {
                let vm = this;

                vm.run_waitMe();

                return axios.get('/seguridad/obtener_saldo_nombre_usuario', {
                    params: {
                        nombre: vm.nombre
                    }
                }).then(function (response) {
                    vm.saldo = response.data;
                }).catch(function (error) {
                    console.error("Ocurrio un error obtenerSaldo")
                }).finally(function () {
                    EasyLoading.hide();
                });
            },

            run_waitMe: function () {

                EasyLoading.show({
                    type: EasyLoading.TYPE.BALL_SCALE_MULTIPLE,
                    text: "Espere por favor, procesando datos..."
                });

            },
        }
    })

})();