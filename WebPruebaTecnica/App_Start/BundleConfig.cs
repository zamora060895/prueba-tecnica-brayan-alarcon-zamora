﻿using System.Web;
using System.Web.Optimization;

namespace WebReclamos
{
    public class BundleConfig
    { 
        public static void RegisterBundles(BundleCollection bundles)
        { 
            bundles.Add(new StyleBundle("~/page/css").Include(
            "~/Css/css.css",
            "~/Css/plugins.bundle.css",
            "~/Css/style.bundle.css",         
            "~/librerias/waitMe-spinner/waitMe.min.css"));
             
            bundles.Add(new ScriptBundle("~/page/jquery").Include(                     
                        "~/Scripts/plugins.bundle.js",
                        "~/Scripts/scripts.bundle.js",                       
                        "~/librerias/waitMe-spinner/waitMe.min.js"
                        ));
 
            //VueJS
            bundles.Add(new ScriptBundle("~/page/vuejs").Include(
                   "~/vueJS/vue.min.js",
                   "~/vueJS/librerias/axios/axios.min.js",
                   "~/vueJS/librerias/veeValidate/vee-validate.min.js",
                   "~/vueJS/librerias/veeValidate/es.js",
                   "~/vueJS/librerias/bootstrap-vue/bootstrap-vue.min.js"));
             
            BundleTable.EnableOptimizations = true;
        }


    }
}