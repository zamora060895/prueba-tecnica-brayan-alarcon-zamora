﻿ 
using Entidades;
using Datos;

namespace Negocio
{
    public class NRuleta
    {

        public bool insertar_usuario(EUsuario eobj)
        {
            return new DRuleta().insertar_usuario( eobj);
        }

        public decimal obtener_saldo_nombre_usuario(string nombre)
        {
            return new DRuleta().obtener_saldo_nombre_usuario(nombre);
        }
    }
}
